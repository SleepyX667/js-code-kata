import Dwellers from './../model/dwellers';

export default class MagicForest {
  private currentPopulation : Array<Dwellers>;
  private finish: boolean;
  private solution: Dwellers;

  constructor(dwellers: Dwellers) {
    this.currentPopulation = [];
    this.currentPopulation.push(dwellers);
  }

  public next() {
    let self = this;
    let temp: Array<Dwellers> = [];

    if (this.currentPopulation.length != 0) {
      this.currentPopulation.forEach(function(dwellers) {
        let result1: Dwellers = dwellers.wolfEatsGoat();
        let result2: Dwellers = dwellers.lionEatsGoat();
        let result3: Dwellers = dwellers.lionEatsWolf();

        if (result1 == null && result2 == null && result3 == null) {
            this.solution = dwellers;
            this.finsih = true;
        }

        temp = self.addDweller(temp, result1);
        temp = self.addDweller(temp, result2);
        temp = self.addDweller(temp, result3);
      }, this);

      this.currentPopulation = temp;
    }
  }

  public life(): Dwellers {
    while(!this.finish) {
      this.next();
    }
    return this.solution;
  }

  private addDweller(itemList: Array<Dwellers>, newItem: Dwellers): Array<Dwellers> {
    if (newItem == null) {
      return itemList;
    }

    let insert:boolean = true;
    itemList.forEach(function(containedDwellers: Dwellers) {
      if (newItem.equals(containedDwellers)) {
        insert = false;
      }
    }, this);

    if(insert) {
      itemList.push(newItem);
    }
    return itemList;
  }
}
