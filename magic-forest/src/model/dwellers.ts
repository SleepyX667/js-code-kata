export default class Dwellers {
  private goats: number;
  private wolves: number;
  private lions: number;

  constructor(goats: number, wolves: number, lions: number) {
    this.goats = goats;
    this.wolves = wolves;
    this.lions = lions;
  }

  public wolfEatsGoat(): Dwellers {
    if ((this.goats > 0) && (this.wolves > 0)) {
      return new Dwellers(this.goats - 1, this.wolves - 1, this.lions + 1);
    }
    return null;
  }

  public lionEatsGoat(): Dwellers {
    if ((this.goats > 0) && (this.lions > 0)) {
      return new Dwellers(this.goats - 1, this.wolves + 1, this.lions - 1);
    }
    return null;
  }

  public lionEatsWolf(): Dwellers {
    if((this.wolves > 0) && (this.lions > 0)) {
      return new Dwellers(this.goats + 1, this.wolves - 1, this.lions - 1);
    }
    return null;
  }

  public toString(): string {
    return "Dwellers[goats: " + this.goats + ", wolves: " + this.wolves + ", lions: " + this.lions + "]";
  }

  public equals(other: Dwellers): boolean {
    if (other == null) {
      return false;
    }
    return (this.goats == other.getGoats()) && (this.wolves == other.getWolves()) && (this.lions == other.getLions());
  }

  public getGoats(): number {
    return this.goats;
  }

  public getWolves(): number {
    return this.wolves;
  }

  public getLions(): number {
    return this.lions;
  }
}
