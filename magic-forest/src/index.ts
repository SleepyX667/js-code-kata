/// <reference path="./model/dwellers.ts" />
/// <reference path="./service/magic-forest.ts" />

//import Dwellers from './model/dwellers';
//import MagicForest from './service/magic-forest';

console.log("Code-Kata: MagicForest");
console.log("----------------------");


let dwellers: Dwellers = new Dwellers(17, 55, 6);
console.log("Dwellers are: " + dwellers.toString());

let magicForest: MagicForest = new MagicForest(dwellers);
let solution:Dwellers = magicForest.life();

console.log("Solution is: " + solution.toString());
