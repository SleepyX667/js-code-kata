import gulp from 'gulp';
import ts from 'gulp-typescript'
import babel from 'gulp-babel';
import del from 'del';
import { exec } from 'child_process';

const PATHS = {
  targetDir: 'target/',
  typeScript: 'src/',
  js6: 'target/ES6/',
  js5: 'target/ES5/'
};

var tsProject = ts.createProject('tsconfig.json');

gulp.task('clean', () => {
  return del(PATHS.targetDir + "**");
});

gulp.task('compile-ts', ['clean'], () => {
  return gulp.src(PATHS.typeScript + "**/*.ts")
    .pipe(tsProject())
    .pipe(gulp.dest(PATHS.js6));
});

gulp.task('compile-js6', ['compile-ts'], () => {
  return gulp.src(PATHS.js6 + "**/*.js")
    .pipe(babel())
    .pipe(gulp.dest(PATHS.js5));
}),

gulp.task('main', ['compile-js6'], (callback) => {
  exec(`node ${PATHS.js5}`, (error, stdout) => {
    console.log(stdout);
    return callback(error);
  });
});

gulp.task('watch', () => {
  gulp.watch(PATHS.typeScript + "**/*.ts", ['main']);
});

gulp.task('default', ['main', 'watch']);
