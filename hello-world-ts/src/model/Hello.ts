class Hello {
  private name: string;

  constructor(name: string) {
    this.name = name;
  }

  speak() {
    if (this.name) {
      return 'Hello ' + this.name;
    } else {
      return 'Hello World';
    }
  }
}

module.exports = Hello;
