const Hello = require('./model/Hello');

const hello1 = new Hello();
const hello2 = new Hello("Peter");

console.log(hello1.speak());
console.log(hello2.speak());
