const Hello = require('./Hello');

class Text {
  constructor() {
     this.hello = new Hello();
  }

  write() {
      return this.hello.speak() + ". Here is a sentence.";
  }
}

module.exports = Text;
