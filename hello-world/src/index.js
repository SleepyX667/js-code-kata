const Hello = require('./model/Hello');
const Text = require('./model/Text')

const hello1 = new Hello();
const hello2 = new Hello("Peter");
const text = new Text();

console.log(hello1.speak());
console.log(hello2.speak());
console.log(text.write());
